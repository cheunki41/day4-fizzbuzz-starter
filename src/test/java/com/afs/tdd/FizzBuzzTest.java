package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {

    @Test
    public void should_say_normal_number_when_sayFizzBuzz_given_normal_number() {
        // given
        Integer number = 1;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("1", actual);
    }

    @Test
    public void should_say_fizz_when_sayFizzBuzz_given_input_is_3() {
        // given
        Integer number = 3;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("Fizz", actual);
    }

    @Test
    public void should_say_fizz_when_sayFizzBuzz_given_input_is_multiple_of_3() {
        // given
        Integer number = 6;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("Fizz", actual);
    }

    @Test
    public void should_say_buzz_when_sayFizzBuzz_given_input_is_5() {
        // given
        Integer number = 5;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("Buzz", actual);
    }

    @Test
    public void should_say_buzz_when_sayFizzBuzz_given_input_is_multiple_of_5() {
        // given
        Integer number = 10;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("Buzz", actual);
    }

    @Test
    public void should_say_fizzbuzz_when_sayFizzBuzz_given_input_is_15() {
        // given
        Integer number = 15;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("FizzBuzz", actual);
    }

    @Test
    public void should_say_fizzbuzz_when_sayFizzBuzz_given_input_is_multiple_of_3_and_5() {
        // given
        Integer number = 30;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("FizzBuzz", actual);
    }

    @Test
    public void should_say_whizz_when_sayFizzBuzz_given_input_is_7() {
        // given
        Integer number = 7;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("Whizz", actual);
    }

    @Test
    public void should_say_whizz_when_sayFizzBuzz_when_given_input_is_multiple_of_7() {
        // given
        Integer number = 14;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("Whizz", actual);
    }

    @Test
    public void should_say_fizzwhizz_when_sayFizzBuzz_when_given_input_is_21() {
        // given
        Integer number = 21;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("FizzWhizz", actual);
    }

    @Test
    public void should_say_fizzwhizz_when_sayFizzBuzz_when_given_input_is_multiple_of_3_and_7() {
        // given
        Integer number = 42;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("FizzWhizz", actual);
    }

    @Test
    public void should_say_buzzwhizz_when_sayFizzBuzz_when_given_input_is_multiple_of_5_and_7() {
        // given
        Integer number = 35;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("BuzzWhizz", actual);
    }

    @Test
    public void should_say_fizzbuzzwhizz_when_sayFizzBuzz_when_given_input_is_multiple_of_3_5_and_7() {
        // given
        Integer number = 105;
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.say(number);

        // then
        assertEquals("FizzBuzzWhizz", actual);
    }

}
